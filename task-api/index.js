const express = require('express');
const mongoose = require('mongoose');

const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.connect("mongodb+srv://chardrichdev:Hayahay2023Chadiks@cluster0.irgko.mongodb.net/b125-tasks?retryWrites=true&w=majority",
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
	}
).then(()=> {
	console.log("Successfully connected to Database");
}).catch((error)=> {
	console.log(error);
})

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: Boolean, 
		default: false
	}
});

const Task = mongoose.model('Task', taskSchema);

// Mini Act

const userSchema = new mongoose.Schema({
	firstName: String,
	lastName: String,
	userName: String,
	password: String
});

const User = mongoose.model(`User`, userSchema);

app.post('/add-task',(req, res)=> {
	let newTask = new Task ({
		name: req.body.name
	});
	newTask.save((error, savedTask)=> {
	if (error){
		console.log(error);
	} else {
		res.send(`New task saved! ${savedTask}`);
	}
	});
});

app.post('/register',(req, res)=> {
	let newUser = new User
	({
		firstName: req.body.firstName,
		lastName: req.body.lastName, 
		userName: req.body.userName, 
		password: req.body.password,
	});
	newUser.save((error, savedUser)=> {
	if (error){
		console.log(error);
	} else {
		res.send(`New user saved! ${savedUser}`);
	}
	});
});

app.get ('/retrieve-tasks', (req, res)=> {
	Task.find({},(error, records)=> {
		if(error){
			console.log(error);
		} else {
			res.send(records);
		}
	});
});

app.get('/retrieve-tasks-done', (req, res)=> {
	Task.find({status:true}, (error, records)=>{
		if (error){
			console.log(error);
		} else {
			res.send(records);
		}
	});
});

app.put('/complete-task/:taskId', (req, res)=> {
	let taskId = req.params.taskId;
	Task.findByIdAndUpdate(taskId, {status:true}, (error, updatedTask)=> {
		if (error){
			console.log(error)
		} else {
			res.send (`Task completed successfully!`);
		}
	});
});

app.delete('/delete-task/:taskId',(req, res)=>{
	let taskId = req.params.taskId;
	Task.findByIdAndDelete(taskId, (error, deletedTask)=>{
		if (error){
			console.log(error)
		} else {
			res.send(`Task deleted!`)
		}
	})
})

app.listen(port, ()=> console.log(`Server is running at port ${port}`));